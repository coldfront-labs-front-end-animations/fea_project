// This file is managed by dropfort/dropfort_build.
// Modifications to this file will be overwritten by default.

module.exports = {
  "*.js": "eslint --fix",
  "*.{php,module,inc,install,test,profile,theme}": "npm run lint:php -- ",
};
