ARG ZOOKEEPER_TAG=latest
FROM zookeeper:$ZOOKEEPER_TAG

RUN chmod -R a+rwx /conf /data /logs /datalog
