ARG PHP_VERSION=7.4
FROM webdevops/php-apache-dev:$PHP_VERSION
ARG ARCHITECTURE="linux/x86_64"
ARG NODE_MAJOR_VERSION="14"
LABEL maintainer="Coldfront Labs Inc. <info@coldfrontlabs.ca>"
WORKDIR /var/www/html

# Allow PHP to read environment variables
ENV fpm.pool.clear_env no
ENV php.variables_order 'EGPCS'
ENV php.realpath_cache_size 256k
ENV PHP_OPCACHE_MEMORY_CONSUMPTION 256M

# Set docroot to match Drupal's
# @todo decide if this is still required given this is set in the docker compose file.
ENV WEB_DOCUMENT_ROOT /var/www/html/web

# Allow lazy-load and other requests on external hostname to work within
# the container instance.
COPY config/apache/listen_8000.conf /opt/docker/etc/httpd/conf.d/listen_8000.conf
COPY config/apache/proxy_vhost.conf /opt/docker/etc/httpd/conf.d/proxy_vhost.conf

# Add setup scripts for Drupal settings files.
COPY scripts/entrypoint/drupal-setup.sh /opt/docker/provision/entrypoint.d/25-drupal-setup.sh
COPY config/drupal/default.settings.php /tmp/default.settings.php
COPY config/drupal/development.services.yml /tmp/development.services.yml

# Add composer bin directory to path.
ENV PATH "$PATH:/var/www/html/bin"

# Update packages.
ENV DEBIAN_FRONTEND=noninteractive
RUN apt update -y; apt upgrade -y

# Fix arm/Apple Silicon support.
RUN if [ "$ARCHITECTURE" = "linux/arm64" ] ; then wget -O "/usr/local/bin/go-replace" "https://github.com/webdevops/goreplace/releases/download/1.1.2/gr-arm64-linux" \
  && chmod +x "/usr/local/bin/go-replace" \
  && "/usr/local/bin/go-replace" --version ; fi

# Add MySQL Client for drush usage.
RUN apt install mariadb-client -y

# Install profiling tools
RUN pecl install xhprof
RUN echo 'extension=xhprof.so' >> /opt/docker/etc/php/php.ini

# Create folder for storing private files outside webroot.
RUN mkdir -p /var/www/files/private

# Add xdebug configuration.
RUN touch /tmp/xdebug_remote_log
RUN chmod a+rw /tmp/xdebug_remote_log

# Add git, drush, phpcs, symfony, composer autocomplete support.
RUN apt install bash-completion -y
RUN composer global require bamarni/symfony-console-autocomplete
ENV SHELL bash
RUN curl -skL https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash -o ~/.git-completion.bash
RUN echo 'source ~/.git-completion.bash' >> ~/.bashrc
RUN mkdir -p /etc/bash_completion.d/composer; composer completion bash > /etc/bash_completion.d/composer/completion.bash || true
#RUN echo 'eval "$(/root/.composer/vendor/bin/symfony-autocomplete --shell bash)"' >> ~/.bashrc
#RUN echo 'eval "$(/root/.composer/vendor/bin/symfony-autocomplete --shell bash drush)"' >> ~/.bashrc
#RUN echo 'eval "$(/root/.composer/vendor/bin/symfony-autocomplete --shell bash drupal)"' >> ~/.bashrc

# Add nodejs, git-lfs and other dev tools.
# @todo make the version of NPM configurable.
RUN curl -sL "https://deb.nodesource.com/setup_$NODE_MAJOR_VERSION.x" -o nodesource_setup.sh
RUN bash nodesource_setup.sh
RUN apt install nodejs build-essential git-lfs jq -y
RUN npm -g i npm
