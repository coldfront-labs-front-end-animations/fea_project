#!/bin/bash

# Ensure the sites/default, files and private folder exists.
mkdir -p "$WEB_DOCUMENT_ROOT/sites/default/files/private"
chown -R application:application "$WEB_DOCUMENT_ROOT/sites/default/files"
chmod -R ug+rwX "$WEB_DOCUMENT_ROOT/sites/default/files"

# Provision a settings.php file if there isn't one already.
if [ ! -f "$WEB_DOCUMENT_ROOT/sites/default/settings.php" ]; then
  cp /tmp/default.settings.php "$WEB_DOCUMENT_ROOT/sites/default/settings.php"
fi

# Provision a development.services.yml file if there isn't one already.
if [ ! -f "$WEB_DOCUMENT_ROOT/sites/default/development.services.yml" ]; then
  cp /tmp/development.services.yml "$WEB_DOCUMENT_ROOT/sites/default/development.services.yml"
fi
