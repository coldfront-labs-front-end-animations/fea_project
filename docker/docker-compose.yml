version: "3.9"

volumes:
  mariadb-data:
  migrate-data:
  solr-data:
  zookeeper-data:
  zookeeper-datalog:
  zookeeper-log:
  xhgui-data:
  node-modules:
  vendor:
  webroot:
  bashhistory:
  bin:

services:
  mariadb:
    image: mariadb:${MARIADB_TAG:-10.6}
    container_name: "${COMPOSE_PROJECT_NAME:-drupal_project}_mariadb"
    stop_grace_period: 30s
    environment:
      MYSQL_ROOT_PASSWORD: ${DB_ROOT_PASSWORD:-password}
      MYSQL_DATABASE: ${DB_NAME:-drupal}
      MYSQL_USER: ${DB_USER:-drupal}
      MYSQL_PASSWORD: ${DB_PASSWORD:-drupal}
      MYSQL_WAIT_TIMEOUT: 6000
    volumes:
      # - ./db/mariadb/init:/docker-entrypoint-initdb.d # Place init .sql file(s) here.
      - mariadb-data:/var/lib/mysql # Use bind mount
    labels:
      - "dropfort.project=${COMPOSE_PROJECT_NAME:-drupal_project}"
      - 'traefik.enable=true'

  migrate:
    image: mariadb:${MARIADB_TAG:-10.6}
    container_name: "${COMPOSE_PROJECT_NAME:-drupal_project}_migrate"
    stop_grace_period: 30s
    environment:
      MYSQL_ROOT_PASSWORD: ${DB_ROOT_PASSWORD:-password}
      MYSQL_DATABASE: ${DB_NAME:-drupal}
      MYSQL_USER: ${DB_USER:-drupal}
      MYSQL_PASSWORD: ${DB_PASSWORD:-drupal}
      MYSQL_WAIT_TIMEOUT: 6000
    volumes:
      # - ./db/mariadb/init:/docker-entrypoint-initdb.d # Place init .sql file(s) here.
      - migrate-data:/var/lib/mysql # Use bind mount
    labels:
      - "dropfort.project=${COMPOSE_PROJECT_NAME:-drupal_project}"
      - 'traefik.enable=true'
    profiles:
      - migrate

  drupal:
    build:
      context: ./
      dockerfile: build/web.dockerfile
      args:
        ARCHITECTURE: "${TARGET_ARCHITECTURE:-linux/x86_64}"
        PHP_VERSION: "${PHP_VERSION:-7.4}"
        NODE_MAJOR_VERSION: "${NODE_MAJOR_VERSION:-14}"
    container_name: "${COMPOSE_PROJECT_NAME:-drupal_project}_drupal"
    environment:
      COLUMNS: 80 # Set 80 columns for docker exec -it.
      COMPOSER_VERSION: "${COMPOSER_VERSION:-2}"
      CONTAINER_DIR: ${CONTAINER_DIR:-/var/www/html}
      DB_DRIVER: ${DB_DRIVER:-mysql}
      DB_HOST: ${DB_HOST:-mariadb}
      DB_NAME: ${DB_NAME:-drupal}
      DB_PASSWORD: ${DB_PASSWORD:-drupal}
      DB_USER: ${DB_USER:-drupal}
      DF_DRUPAL_WEBROOT: ${DF_DRUPAL_WEBROOT:-web}
      POSTFIX_RELAYHOST: mailhog:1025
      SSH_AUTH_SOCK: "/run/host-services/ssh-auth.sock"
      WEB_ALIAS_DOMAIN: '*.localhost'
      WEB_DOCUMENT_ROOT: ${CONTAINER_DIR:-/var/www/html}/${DF_DRUPAL_WEBROOT:-web}
      XDEBUG_SESSION: vscode
      php.xdebug.client_host: "${COMPOSE_PROJECT_NAME:-drupal_project}_drupal"
      php.xdebug.client_port: 9003
      php.xdebug.discover_client_host: 0
      php.xdebug.start_with_request: "yes"
    volumes:
      - ../:${CONTAINER_DIR:-/var/www/html}:cached
      - ${SSH_AUTH_SOCK:-/run/host-services/ssh-auth.sock}:/run/host-services/ssh-auth.sock
      - ~/.ssh/config:/root/.ssh/config
      - node-modules:${CONTAINER_DIR:-/var/www/html}/node_modules
      - vendor:${CONTAINER_DIR:-/var/www/html}/vendor
      - webroot:${CONTAINER_DIR:-/var/www/html}/${DF_DRUPAL_WEBROOT:-web}
      - bin:${CONTAINER_DIR:-/var/www/html}/bin
      - bashhistory:/commandhistory
    labels:
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME:-drupal_project}_drupal.rule=Host(`drupal.${PROJECT_BASE_URL:-dflocal.net}`)"
      - "dropfort.project=${COMPOSE_PROJECT_NAME:-drupal_project}"
      - 'traefik.enable=true'

  mailhog:
    image: mailhog/mailhog
    container_name: "${COMPOSE_PROJECT_NAME:-drupal_project}_mailhog"
    labels:
      - "traefik.http.services.${COMPOSE_PROJECT_NAME:-drupal_project}_mailhog.loadbalancer.server.port=8025"
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME:-drupal_project}_mailhog.rule=Host(`mailhog.${PROJECT_BASE_URL:-dflocal.net}`)"
      - "dropfort.project=${COMPOSE_PROJECT_NAME:-drupal_project}"
      - 'traefik.enable=true'


  traefik:
    image: traefik:latest
    container_name: "${COMPOSE_PROJECT_NAME:-drupal_project}_traefik"
    # @see https://community.traefik.io/t/multiple-docker-compose-treafik-instances/11299
    command:
      - '--api.insecure=true'
      - '--providers.docker'
      # Do not expose containers unless explicitly told so
      - '--providers.docker.exposedbydefault=false'
      - '--providers.docker.constraints=Label(`dropfort.project`,`${COMPOSE_PROJECT_NAME}`)'
    ports:
      - "${HOST_PORT:-8000}:80"
      - "${MANAGE_PORT:-8080}:8080"
    volumes:
      - ${TRAEFIK_DOCKER_SOCKET:-/var/run/docker.sock}:/var/run/docker.sock

  ## Memcache ##
  ### profiles: [ memcache ]
  memcache:
    image: memcached:${MEMCACHE_TAG:-latest}
    command: ["-m", "${MEMCACHE_MEMORY:-1024}"]
    container_name: "${COMPOSE_PROJECT_NAME:-drupal_project}_memcache"
    labels:
      - "dropfort.project=${COMPOSE_PROJECT_NAME:-drupal_project}"
      - 'traefik.enable=true'
    profiles:
      - memcache

  ## Search API Solr with SolrCloud ###
  ### profiles: [search_api, solr]
  solr:
    build:
      context: ./
      dockerfile: build/solr.dockerfile
    container_name: "${COMPOSE_PROJECT_NAME:-drupal_project}_solr"
    # @see https://github.com/docker-solr/docker-solr/issues/246#issuecomment-802056386
    # Uses sample security.json from https://solr.apache.org/guide/6_6/authentication-and-authorization-plugins.html
    # u/p: solr/SolrRocks
    # DO NOT USE IN PRODUCTION
    command: bash -c "bin/solr zk cp file:/ssl/security.json zk:/security.json && exec solr-foreground"
    volumes:
      - solr-data:/var/solr
    environment:
      SOLR_HEAP: 1024m
      SOLR_SSL_CHECK_PEER_NAME: false
      SOLR_SSL_ENABLED: false
      SOLR_SSL_KEY_STORE: /ssl/solr-ssl.keystore.jks
      SOLR_SSL_KEY_STORE_PASSWORD: secret
      SOLR_SSL_KEY_STORE_TYPE: JKS
      SOLR_SSL_NEED_CLIENT_AUTH: false
      SOLR_SSL_TRUST_STORE: /ssl/solr-ssl.keystore.jks
      SOLR_SSL_TRUST_STORE_PASSWORD: secret
      SOLR_SSL_TRUST_STORE_TYPE: JKS
      SOLR_SSL_WANT_CLIENT_AUTH: false
      ZK_HOST: zookeeper:2181
    labels:
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME:-drupal_project}_solr.rule=Host(`solr.${PROJECT_BASE_URL:-dflocal.net}`)"
      - "dropfort.project=${COMPOSE_PROJECT_NAME:-drupal_project}"
      - 'traefik.enable=true'
    depends_on:
      - zookeeper
      - solr-initializer
    profiles:
      - search_api
      - solr

  zookeeper:
    build:
      context: ./
      dockerfile: build/zookeeper.dockerfile
    container_name: "${COMPOSE_PROJECT_NAME:-drupal_project}_zookeeper"
    hostname: zookeeper
    environment:
      ZOO_MY_ID: 1
      ZOO_SERVERS: server.1=0.0.0.0:2888:3888;2181
      # borrow from https://www.drupal.org/docs/develop/local-server-setup/docker-development-environments/docker-with-solr-cloud-integration/docker-configuration
      ZOO_4LW_COMMANDS_WHITELIST: mntr,conf,ruok,srvr
    volumes:
      - zookeeper-data:/data
      - zookeeper-datalog:/datalog
      - zookeeper-log:/logs
    labels:
      - "dropfort.project=${COMPOSE_PROJECT_NAME:-drupal_project}"
      - 'traefik.enable=true'
    profiles:
      - search_api
      - solr

  solr-initializer:
    image: alpine
    container_name: "${COMPOSE_PROJECT_NAME:-drupal_project}_solr-initializer"
    restart: "no"
    entrypoint: |
      /bin/sh -c "chown -R 8983:8983 /var/solr"
    volumes:
      - solr-data:/var/solr
    profiles:
      - search_api
      - solr

  ## Selenium Grid Browser Testing
  ### profiles: [debug, selenium, firefox, chrome, edge]
  selenium:
    image: selenium/hub
    container_name: ${COMPOSE_PROJECT_NAME:-drupal_project}_selenium
    environment:
      START_XVFB: false
      GRID_MAX_SESSION: 16
      GRID_BROWSER_TIMEOUT: 3000
      GRID_TIMEOUT: 3000
    labels:
      - "traefik.http.services.${COMPOSE_PROJECT_NAME:-drupal_project}_selenium.loadbalancer.server.port=4444"
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME:-drupal_project}_selenium.rule=Host(`selenium.${PROJECT_BASE_URL:-dflocal.net}`)"
      - "dropfort.project=${COMPOSE_PROJECT_NAME:-drupal_project}"
      - 'traefik.enable=true'
    profiles:
      - debug
      - selenium
      - firefox
      - edge
      - chrome

  chrome:
    image: selenium/node-chrome
    container_name: ${COMPOSE_PROJECT_NAME:-drupal_project}_selenium_chrome
    shm_size: 2g
    depends_on:
      - selenium
    environment:
      SE_EVENT_BUS_HOST: selenium
      SE_EVENT_BUS_PUBLISH_PORT: ${SE_EVENT_BUS_PUBLISH_PORT:-4442}
      SE_EVENT_BUS_SUBSCRIBE_PORT: ${SE_EVENT_BUS_SUBSCRIBE_PORT:-4443}
      SE_NODE_SESSION_TIMEOUT: ${SE_NODE_SESSION_TIMEOUT:-60}
    labels:
      - "dropfort.project=${COMPOSE_PROJECT_NAME:-drupal_project}"
      - 'traefik.enable=true'
    profiles:
      - debug
      - selenium
      - chrome

  firefox:
    image: selenium/node-firefox
    container_name: ${COMPOSE_PROJECT_NAME:-drupal_project}_selenium_firefox
    shm_size: 2g
    depends_on:
      - selenium
    environment:
      SE_EVENT_BUS_HOST: selenium
      SE_EVENT_BUS_PUBLISH_PORT: ${SE_EVENT_BUS_PUBLISH_PORT:-4442}
      SE_EVENT_BUS_SUBSCRIBE_PORT: ${SE_EVENT_BUS_SUBSCRIBE_PORT:-4443}
      SE_NODE_SESSION_TIMEOUT: ${SE_NODE_SESSION_TIMEOUT:-60}
    labels:
      - "dropfort.project=${COMPOSE_PROJECT_NAME:-drupal_project}"
      - 'traefik.enable=true'
    profiles:
      - debug
      - selenium
      - firefox

  edge:
    image: selenium/node-edge
    container_name: ${COMPOSE_PROJECT_NAME:-drupal_project}_selenium_edge
    shm_size: 2g
    depends_on:
      - selenium
    environment:
      SE_EVENT_BUS_HOST: selenium
      SE_EVENT_BUS_PUBLISH_PORT: ${SE_EVENT_BUS_PUBLISH_PORT:-4442}
      SE_EVENT_BUS_SUBSCRIBE_PORT: ${SE_EVENT_BUS_SUBSCRIBE_PORT:-4443}
      SE_NODE_SESSION_TIMEOUT: ${SE_NODE_SESSION_TIMEOUT:-60}
    labels:
      - "dropfort.project=${COMPOSE_PROJECT_NAME:-drupal_project}"
      - 'traefik.enable=true'
    profiles:
      - debug
      - selenium
      - edge

  ## Performance Profiling with XHProf/XHGui
  ### profiles: [debug, profiling]
  xhgui:
    image: xhgui/xhgui
    container_name: "${COMPOSE_PROJECT_NAME:-drupal_project}_xhgui"
    volumes:
      - ./config/xhgui/config:/var/www/xhgui/config
      - ./config/xhgui/nginx.conf:/etc/nginx/http.d/default.conf:ro
    environment:
      - XHGUI_MONGO_HOSTNAME=${XHGUI_MONGO_HOSTNAME:-xhgui_mongodb}
      - XHGUI_MONGO_DATABASE=xhprof
    labels:
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME:-drupal_project}_xhgui.rule=Host(`xhgui.${PROJECT_BASE_URL:-dflocal.net}`)"
      - "dropfort.project=${COMPOSE_PROJECT_NAME:-drupal_project}"
      - 'traefik.enable=true'
    depends_on:
      - xhgui_mongodb
    profiles:
      - debug
      - profiling

  xhgui_mongodb:
    image: percona/percona-server-mongodb:3.6
    container_name: "${COMPOSE_PROJECT_NAME:-drupal_project}_xhgui_mongodb"
    # (case sensitive) engine: mmapv1, rocksdb, wiredTiger, inMemory
    command: --storageEngine=wiredTiger
    environment:
      - MONGO_INITDB_DATABASE=xhprof
    volumes:
      # By default profiling runs are reset on restart. Comment out the
      # following line to persist your runs.
      # - ./data/db/xhgui_mongodb/init.d:/docker-entrypoint-initdb.d
      - xhgui-data:/data/db
    labels:
      - "dropfort.project=${COMPOSE_PROJECT_NAME:-drupal_project}"
      - 'traefik.enable=true'
    profiles:
      - debug
      - profiling

  ## API Integration Testing
  ### profiles: [debug, api]
  requestbaskets:
    image: darklynx/request-baskets
    container_name: "${COMPOSE_PROJECT_NAME:-drupal_project}_requestbasket"
    hostname: requestbasket
    labels:
      - "traefik.http.services.${COMPOSE_PROJECT_NAME:-drupal_project}_requestbaskets.loadbalancer.server.port=55555"
      - "traefik.http.routers.${COMPOSE_PROJECT_NAME:-drupal_project}_requestbaskets.rule=Host(`requestbaskets.${PROJECT_BASE_URL:-dflocal.net}`)"
      - "dropfort.project=${COMPOSE_PROJECT_NAME:-drupal_project}"
      - 'traefik.enable=true'
    profiles:
      - debug
      - api
